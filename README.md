# smartflash

An automatic flow3r mass flash tool

## Description
smartflash detects flow3rs which are attached via USB and in Espressif USB JTAG/serial mode

It then proceeds to automatically flash all files which are needed to use it.

## Usage

Assumes FLASHcli is already running

 * connect flow3r while holding the left physical button down (i.e. enter bootloader)
 * flow3r should turn on a blue led near the usb to indicate flashing has started
 * wait, possibly observer FLASHcli output
 * flow3r should start blinking green to indicate flasing is done

If flow3r stays blue for longer than 1-2min, some error occurred. Disconnect and start at the top


## Prerequisites

Requires perl installed the binary files in app/ and recovery/  (see below to build/gather)

 * app/Go_Green.bin
 * app/Go_Blue.bin
 * app/flow3r.bin
 * recovery/flow3r-recovery.bin
 * recovery/bootloader.bin
 * recovery/partition-table.bin

run as

```
./FLASHcli
```


## Building the firmware

You can check out and build the firmware & requisites yourself.

"make build" tries to automate it, this requires

git@git.flow3r.garden:flow3r/flow3r-firmware.git and
git@git.flow3r.garden:flow3r/flow3r-minimal.git checked out and in a buildable state.

standard flow3r prerequisites (e.g. esp-idf) also.


## Collecting the required files

Run "make gather" to fetch all the files from the previous build step into the app/ and recovery/ directories

## Starting the process:
"make run" will start the CLI too

### Command line options
 * -t -- disable "gui" element at the top
 * -d -- enable debug logging to /tmp/debuglog.
 * -p -- change usb sysfs path (default /sys/bus/usb/devices)
 * -f -- debug option (-f err" makes all flash fail), "-f ok" makes all flash noop success)
