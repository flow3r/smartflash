# Makefile to create filesystem image and flash rad10 badge

default: gather
all: build gather run

fdir?=../firmware
rdir?=../firmware/recovery
adir?=../flow3r-minimal


gather:
	@echo
	@echo '### Gathering contents flashing'
	mkdir -p recovery app
	cp ${adir}/build/Go_Green.bin app/
	cp ${adir}/build/Go_Blue.bin app/
	cp ${fdir}/build/flow3r.bin app/
	cp ${rdir}/build/bootloader/bootloader.bin recovery/
	cp ${rdir}/build/flow3r-recovery.bin recovery/
	cp ${rdir}/build/partition_table/partition-table.bin recovery/

run:
	./FLASHgui

### Rebuild everything
build:
	@echo
	@echo '### Building everything'
	cd ${fdir} && idf.py build
	cd ${rdir} && idf.py build
	cd ${adir} && idf.py build

clean:
	#$(RM) -rf $(fsdir).* $(fsdir)
	#$(RM) $(fsimg).* $(hdrimg) $(fullimg).* flashapp.dfu


### Building individual images with different random data per badge

### Targets for FLASHgui
flash:
	esptool.py --chip esp32s3 -p ${UTTY} --no-stub load_ram app/Go_Blue.bin
	@sleep 0.5
	esptool.py --chip esp32s3 -p ${UTTY} --before=default_reset --after no_reset erase_flash 2>/dev/null
	esptool.py --chip esp32s3 -p ${UTTY} --before=default_reset --after=no_reset write_flash --flash_mode dio --flash_freq 80m --flash_size 16MB 0x0 recovery/bootloader.bin 0x10000 recovery/flow3r-recovery.bin 0x8000 recovery/partition-table.bin
	esptool.py --chip esp32s3 -p ${UTTY} --before=default_reset --after=no_reset write_flash --flash_mode dio --flash_freq 80m --flash_size 16MB 0x90000 app/flow3r.bin
	esptool.py --chip esp32s3 -p ${UTTY} --no-stub load_ram app/Go_Green.bin

flasherr:
	@sleep 0.5
	exit 1

flashok:
	@sleep 0.5
